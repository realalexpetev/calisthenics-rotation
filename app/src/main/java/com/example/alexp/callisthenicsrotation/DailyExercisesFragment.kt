package com.example.alexp.callisthenicsrotation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.alexp.callisthenicsrotation.ui.components.ExerciseRecyclerViewAdapter
import com.example.alexp.callisthenicsrotation.ui.components.model.Exercise
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_daily_exercise.*

class DailyExercisesFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_daily_exercise, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val exercisesList: MutableList<Exercise>

        val exerciseFileString: String = resources.openRawResource(R.raw.exercises).bufferedReader().use { it.readText() }

        exercisesList = (Gson().fromJson(exerciseFileString, Array<Exercise>::class.java)).toMutableList()

        recyclerView.apply {
            recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
            adapter = ExerciseRecyclerViewAdapter(exercisesList)
        }
    }
}