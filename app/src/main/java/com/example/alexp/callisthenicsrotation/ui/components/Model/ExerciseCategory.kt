package com.example.alexp.callisthenicsrotation.ui.components.model

enum class ExerciseCategory(category: String) {
    HANDSTAND_PUSHUPS("Handstand Pushups"),
    PUSHUPS("Pushups"),
    SQUATS("Squats"),
    PULLUPS("Pullups"),
    BRIDGES("Bridges"),
}
