package com.example.alexp.callisthenicsrotation.ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.alexp.callisthenicsrotation.R

/**
 * Created by alexp on 9/8/2018
 */

class ExerciseDisplayView (context: Context, attributeSet: AttributeSet): LinearLayout(context, attributeSet) {
    init {

        val view = LayoutInflater.from(context).inflate(R.layout.exercise_display,this,false)
        addView(view)
    }
}