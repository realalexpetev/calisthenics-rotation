package com.example.alexp.callisthenicsrotation.ui.components.model

data class Exercise(
        val description: String?,
        val exerciseCategory: String?,
        val exerciseName: String?,
        val id: Int?,
        val proficiencyLevel: Int?,
        val reps: Int?,
        val sets: Int?,
        val stepNumber: Int?
)