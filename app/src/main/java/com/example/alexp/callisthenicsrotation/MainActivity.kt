package com.example.alexp.callisthenicsrotation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.example.alexp.callisthenicsrotation.ui.components.DailyExercisePageAdapter
import com.example.alexp.callisthenicsrotation.ui.components.ExerciseRecyclerViewAdapter
import com.example.alexp.callisthenicsrotation.ui.components.model.Exercise
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager = pager
        setupViewPager(viewPager)

        tabLayout = tabs
        tabLayout.setupWithViewPager(viewPager)

        }

    private fun setupViewPager(viewPager: ViewPager){
        val adapter = DailyExercisePageAdapter(supportFragmentManager)
        adapter.addFragment(DailyExercisesFragment(), "Monday")
        adapter.addFragment(DailyExercisesFragment(), "Tuesday")
        adapter.addFragment(DailyExercisesFragment(), "Wednesday")
        adapter.addFragment(DailyExercisesFragment(), "Thursday")
        adapter.addFragment(DailyExercisesFragment(), "Friday")
        adapter.addFragment(DailyExercisesFragment(), "Saturday")
        adapter.addFragment(DailyExercisesFragment(), "Sunday")
        viewPager.adapter = adapter
    }
}
