package com.example.alexp.callisthenicsrotation.ui.components

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.alexp.callisthenicsrotation.R
import com.example.alexp.callisthenicsrotation.ui.components.model.Exercise
import com.example.alexp.callisthenicsrotation.ui.components.model.ExerciseCategory
import kotlinx.android.synthetic.main.exercise_display.view.*


class ExerciseRecyclerViewAdapter(private val exercises: List<Exercise>): RecyclerView.Adapter<ExerciseHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.exercise_display, parent, false)

        return ExerciseHolder(view)
    }

    override fun getItemCount(): Int {
        return exercises.size
    }

    override fun onBindViewHolder(holder: ExerciseHolder, position: Int) {

        val currentExercise = exercises[position]

        holder.exerciseCategory.text = currentExercise.exerciseCategory
        holder.exerciseName.text = currentExercise.exerciseName
        holder.proficiencyLevel.text = "Proficiency lvl: " + currentExercise.proficiencyLevel.toString()
        holder.reps.text = currentExercise.reps.toString() + " reps"
        holder.stepNumber.text = "Step №" + currentExercise.stepNumber.toString()
        holder.sets.text = currentExercise.sets.toString() + " sets"

        holder.itemView.setOnClickListener {
            Toast.makeText(holder.itemView.context, currentExercise.exerciseName, Toast.LENGTH_SHORT).show()
        }

    }

}

class ExerciseHolder(view: View): RecyclerView.ViewHolder(view) {
    val exerciseCategory = itemView.exerciseCategory
    val exerciseName = itemView.exerciseName
    val proficiencyLevel = itemView.exerciseProficiency
    val reps = itemView.exerciseReps
    val sets = itemView.exerciseSets
    val stepNumber = itemView.exerciseStep
}